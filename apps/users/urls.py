#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/7 20:07
# @Author  : maxinehehe
# @Site    : 
# @File    : urls.py
# @Software: PyCharm


from django.conf.urls import url,include

from .views import UserinfoView, UploadImageView, UpdatePwdView, SendEmailCodeView
from .views import UpdateEmailView, MyCourseView, MyFavOrgView, MyFavTeacherView, MyFavCourseView
from .views import MyMessageView

# 柯城列表页的配置
# 可以 先写url 再写view
urlpatterns = [
    # 用户信息
    url(r'^info/$',UserinfoView.as_view() , name='user_info'), # 刚进入更开课 也是课程列表

    # 用户头像上传
    url(r'^image/$',UploadImageView.as_view() , name='image_upload'), # 刚进入更开课 也是课程列表

    # 用户个人中心修改密码
    url(r'^update/pwd/$',UpdatePwdView.as_view() , name='update_pwd'), # 刚进入更开课 也是课程列表

    # 发送邮箱验证码
    url(r'^sendemail_code/$',SendEmailCodeView.as_view() , name='sendemail_code'),

    # 修改邮箱 即收到邮箱验证码之后进行修改
    url(r'^update_email/$',UpdateEmailView.as_view() , name='update_email'),

    # 个人中心 我的课程
    url(r'^mycourse/$',MyCourseView.as_view() , name='mycourse'),

    # 我收藏的课程机构
    url(r'^myfav/org/$',MyFavOrgView.as_view() , name='myfav_org'),

    # 我收藏的课程讲师
    url(r'^myfav/teacher/$',MyFavTeacherView.as_view() , name='myfav_teacher'),

    # 我收藏的课程
    url(r'^myfav/course/$',MyFavCourseView.as_view() , name='myfav_course'),

    url(r'^mymessage/$',MyMessageView.as_view() , name='mymessage'),

    ]