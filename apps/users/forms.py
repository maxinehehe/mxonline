#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/15 10:09
# @Author  : maxinehehe
# @Site    : 
# @File    : forms.py
# @Software: PyCharm
from django import forms
from captcha.fields import CaptchaField
from .models import UserProfile

'''
处理用户登录
'''
class LoginForm(forms.Form):
    username = forms.CharField(required=True)  # 该字段不能为空
    password = forms.CharField(required=True, min_length=5)  # 该字段不能为空  减少数据库查询负担
# 应用 在post中


class RegisterForm(forms.Form):
    # 注册表单
    # email password等字段必须与HTML文件中name保持一致
    email = forms.EmailField(required=True)   # 对email字段做验证
    password = forms.CharField(required=True, min_length=5)  # 该字段不能为空  减少数据库查询负担
    captcha = CaptchaField(error_messages={"invalid":u"验证码错误"})


class ForgetrForm(forms.Form):
    # 忘记密码表单
    email = forms.EmailField(required=True)   # 对email字段做验证
    captcha = CaptchaField(error_messages={"invalid":u"验证码错误"})


class ModifyPwdForm(forms.Form):
    # 注册表单
    # email password等字段必须与HTML文件中name保持一致
    # email = forms.EmailField(required=True)   # 对email字段做验证
    password1 = forms.CharField(required=True, min_length=5)  # 该字段不能为空  减少数据库查询负担
    password2 = forms.CharField(required=True, min_length=5)  # 该字段不能为空  减少数据库查询负担


class UploadImageForm(forms.ModelForm):
    '''
    用于获取form表单中的图片数据
    '''
    # 定义meta信息
    class Meta():
        model = UserProfile
        # 可以选取只要某些字段 验证时默认不为空  从form中取出图像文件
        fields = ['image' ]


class UserInfoForm(forms.ModelForm):
    # 定义meta信息
    class Meta():
        model = UserProfile
        # 可以选取只要某些字段 验证时默认不为空  从form中取出图像文件
        fields = ['nick_name', 'gender', 'birday', 'address', 'mobile' ]