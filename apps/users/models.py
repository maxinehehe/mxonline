#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser


# 继承原有数据库用户表
# Create your models here.
# user表 应该首先被设计
# ORM

# 用户 model
class UserProfile(AbstractUser):
    # 昵称
    nick_name = models.CharField(max_length=50, verbose_name=u"昵称", default=u"")
    # 生日
    birday = models.DateField(verbose_name=u"生日", null=True, blank=True)
    # 性别
    gender = models.CharField(max_length=7, choices=(("male",u"男"), ("female", u"女")), default="female", verbose_name=u"性别")
    # 地址 区域
    address = models.CharField(max_length=100, default=u"", verbose_name=u"地址")
    # 手机
    mobile = models.CharField(max_length=11, null=True, blank=True, verbose_name=u"手机号码")
    # 头像
    image = models.ImageField(upload_to="image/%y/%m", default=u"image/default.png", max_length=100, verbose_name=u"头像")
    '''
        使用strftime（）函数
        如果觉得以上方式太僵硬，万一文件重名了，那就会有各种问题了，为了避免重名，django在upload_to上内置了strftime（）函数
        # models.py
        class User(models.Model):
            avatar = ImageField(upload_to = 'avatar/%Y/%m/%d/')
        这样子的方式，%Y、%m、%d分别表示年、月、日
    '''
    # 定义meta信息
    class Meta:
        verbose_name = u"用户"
        verbose_name_plural = verbose_name   # 复数处理

    # 重载方法 若不重载 在创建UserProfile实例时,就不能打印我们自定义的字符串
    def __unicode__(self):
        # __unicode()方法告诉python如何实现对象的unicode表示。
        return self.username

    def unread_nums(self):
        # 获取用户未读取消息的数量
        from operation.models import UserMessage
        # curr_user_messages = UserMessage.objects.filter(user=self.id, has_read=False).count()
        # default_user_messages =
        # if curr_user_messages == 0:

        return UserMessage.objects.filter(user=self.id, has_read=False).count()\
               # +UserMessage.objects.filter(user=0, has_read=False).count()

# 邮箱验证码 model设计
class EmailVerifyRecord(models.Model):
    # 均不为空
    code = models.CharField(max_length=20, verbose_name=u"验证码")
    email = models.EmailField(max_length=50, verbose_name=u"邮箱")
    # 邮箱验证码 注册与找回密码时均可用
    send_type = models.CharField(choices=(("register",u"注册"), ("forget", u"找回密码"),("update_email", u"修改邮箱")),
                                 max_length=30, verbose_name=u"验证码类型")
    # 添加send_time 用于 处理过期的验证
    # 注意 不能用datetime.now() 这样会根据类编译的时间进行改写
    send_time = models.DateTimeField(default=datetime.now, verbose_name=u"发送时间")

    class Meta:
        verbose_name = u"邮箱验证码"
        verbose_name_plural = verbose_name

    # 重载方法 若不重载 在创建UserProfile实例时,就不能打印我们自定义的字符串
    def __unicode__(self):
        # __unicode()方法告诉python如何实现对象的unicode表示。
        return '{0}({1})'.format(self.code, self.email)

# 轮播图
class Banner(models.Model):
    # 轮播图标题
    title = models.CharField(max_length=100, verbose_name=u"标题")
    # 轮播图路径
    image = models.ImageField(upload_to="banner/%Y/%m", verbose_name=u"轮播图", max_length=100)
    # 跳转url
    url = models.URLField(max_length=200, verbose_name=u"访问地址")
    # 轮播图顺序
    index = models.IntegerField(default=100, verbose_name=u"轮播顺序")
    # 当前记录生成时间
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"轮播图"
        verbose_name_plural = verbose_name # 不加s
