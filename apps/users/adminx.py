#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/12 14:20
# @Author  : maxinehehe
# @Site    : 
# @File    : adminx.py
# @Software: PyCharm
'''
xadmin 会自动搜寻各app下xadmin.py文件 用于注册
此外将全站的配置放到user下adminx.py文件
'''

import xadmin
from  xadmin import views
from xadmin.plugins.auth import UserAdmin
from .models import EmailVerifyRecord, Banner, UserProfile
from xadmin.layout import Fieldset, Main, Side, Row

class UserProfileAdmin(UserAdmin):
    pass

class BaseSetting(object):
    '''
    指定写法
    '''
    # 使用主题功能
    enable_themes = True
    #
    use_bootswatch = True

class GlobalSetting(object):
    # 页面左上角
    site_title = u"微慕课后台管理系统"
    # 页脚
    site_footer = u"WIMOOC在线网"
    # 设置滚脚 二级目录
    menu_style = "accordion"

class EmailVerifyRecordAdmin(object):  # 直接继承顶层类 这一点与admin有所区别
    # 更改显示字段
    list_display = ['code', 'email', 'send_type', 'send_time']
    # 添加搜索字段
    search_fields = ['code', 'email', 'send_type']
    # 添加过滤器
    list_filter = ['code', 'email', 'send_type', 'send_time']
    # model_icon = 'fas fa-address-book-o'
    # model_icon = 'fas fa-address-book'

class BannerAdmin(object):
    # 更改显示字段
    list_display = ['title', 'image', 'url', 'index', 'add_time']
    # 添加搜索字段
    search_fields = ['title', 'image', 'url', 'index']
    # 添加过滤器
    list_filter = ['title', 'image', 'url', 'index', 'add_time']

# 注册邮箱验证码  将 A 注册到 B
# from django.contrib.auth.models import User
# xadmin.site.unregister(User)
xadmin.site.register(EmailVerifyRecord, EmailVerifyRecordAdmin)
# 注册轮播图
xadmin.site.register(Banner, BannerAdmin)

# xadmin.site.register(UserProfile, UserProfileAdmin)

# BaseSetting 注册方式 要与iew绑定
xadmin.site.register(views.BaseAdminView, BaseSetting)

# 注册页头 脚信息事件
xadmin.site.register(views.CommAdminView, GlobalSetting)