#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/12 14:20
# @Author  : maxinehehe
# @Site    :
# @File    : adminx.py
# @Software: PyCharm
from __future__ import unicode_literals

from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = u"用户信息"   # 易于阅读的名字