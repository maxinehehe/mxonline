#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/12 14:20
# @Author  : maxinehehe
# @Site    :
# @File    : adminx.py
# @Software: PyCharm

from __future__ import unicode_literals

from django.apps import AppConfig


# 配置app显示名称
class CoursesConfig(AppConfig):
    name = 'courses'
    verbose_name = u"课程管理"
