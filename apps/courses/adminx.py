#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/12 14:59
# @Author  : maxinehehe
# @Site    : 
# @File    : adminx.py
# @Software: PyCharm
import xadmin

from .models import Course, Lesson, Video, CourseResource
from organization.models import CourseOrg

# 分别创建admin

# 联结 课程与章节
class LessonInline(object):
    model = Lesson
    extra = 0

class CourseResourceInline(object):
    model = CourseResource
    extra = 0

class CourseAdmin(object):
    # 更改显示字段
    list_display = ['name', 'desc', 'detail', 'degree', 'learn_times', 'students', 'fav_nums',  'click_nums', 'get_zj_nums']
    # 添加搜索字段
    search_fields = ['name', 'desc', 'detail', 'degree', 'learn_times', 'students', 'fav_nums',  'click_nums']
    # 添加过滤器
    list_filter = ['name', 'desc', 'detail', 'degree', 'learn_times', 'students', 'fav_nums',  'click_nums']
    ordering = ['-click_nums'] # 根据点击数排序
    readonly_fields = ['click_nums']  # 只读
    list_editable = ['desc', 'degree']  # 可以直接进行编辑 无需进入修改编辑页面
    # exclude = ['fav_nums']  # 隐藏 不可见
    inlines = [LessonInline, CourseResourceInline]
    # refresh_times = [3, 5]  自动刷新 每3秒 每5秒

    def save_models(self):
        # 在保存课程时统计课程机构的课程数
        # 即再添加课程时 就进行统计该课程属于哪个机构 将该机构的课程数统计下来
        obj = self.new_obj
        obj.save()
        if obj.course_org is not None:
            course_org = obj.course_org
            course_org.course_nums = Course.objects.filter(course_org=course_org).count()
            course_org.save()



class LessonAdmin(object):
    # 更改显示字段
    list_display = ['course', 'name', 'add_time']
    # 添加搜索字段
    search_fields = ['course', 'name']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['course__name', 'name', 'add_time'] # course__name表示利用【外键】course的name字段进行过滤


class VideoAdmin(object):
     # 更改显示字段
    list_display = ['lesson', 'name', 'add_time']
    # 添加搜索字段
    search_fields = ['lesson', 'name']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['lesson', 'name', 'add_time']


class CourseResourceAdmin(object):
    # 更改显示字段
    list_display = ['course', 'name', 'download', 'add_time']
    # 添加搜索字段
    search_fields = ['course', 'name', 'download']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['course', 'name', 'download', 'add_time']
# 注册课程事件
xadmin.site.register(Course, CourseAdmin)
# 注册章节基本信息事件 【存在对外键的处理】
xadmin.site.register(Lesson, LessonAdmin)
# 注册视频事件
xadmin.site.register(Video, VideoAdmin)
# 注册课程资源事件
xadmin.site.register(CourseResource, CourseResourceAdmin)