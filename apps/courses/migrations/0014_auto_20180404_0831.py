# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2018-04-04 08:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0013_auto_20180404_0828'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='teacher_tell',
            field=models.TextField(default='', max_length=300, null=True, verbose_name='\u8001\u5e08\u544a\u8bc9\u4f60\u80fd\u5b66\u5230\u4ec0\u4e48\uff1f'),
        ),
        migrations.AlterField(
            model_name='course',
            name='youneed_know',
            field=models.TextField(default='', max_length=300, null=True, verbose_name='\u8bfe\u7a0b\u987b\u77e5'),
        ),
    ]
