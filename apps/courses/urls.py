#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/3/29 12:14
# @Author  : maxinehehe
# @Site    : 
# @File    : urls.py
# @Software: PyCharm

from django.conf.urls import url,include

from .views import CourseListView, CourseDetailView, CourseInfoView, CommentsView, AddCommentsView, VideoPlayView


# 柯城列表页的配置
# 可以 先写url 再写view
urlpatterns = [
    # 课程列表页
     url(r'^list/$',CourseListView.as_view() , name='course_list'), # 刚进入更开课 也是课程列表
     # 课程详情页
     url(r'^detail/(?P<course_id>\d+)/$',CourseDetailView.as_view() , name='course_detail'),
     # 课程详情之章节信息
     url(r'^info/(?P<course_id>\d+)/$',CourseInfoView.as_view() , name='course_info'),
     # 课程评论
     url(r'^comment/(?P<course_id>\d+)/$',CommentsView.as_view() , name='course_comments'),
     # 添加课程评论  异步请求
     url(r'^add_comment/$',AddCommentsView.as_view() , name='add_comment'),
     # 视频播放配置
     url(r'^video/(?P<video_id>\d+)/$',VideoPlayView.as_view() , name='video_play'),
    ]