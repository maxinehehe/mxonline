#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime
import os
from organization.models import CourseOrg, Teacher

# current_path = os.path.dirname(os.path.realpath(__file__))+'/'
# print "----------------------------->",current_path
# Create your models here.
'''
课程models   即对课程页面的设计  包括表之间的对应关系
'''


class Course(models.Model):
    # 增添属性 新添加字段 必须null=True以防止新添加字段出错
    course_org = models.ForeignKey(CourseOrg, verbose_name=u"课程机构", null=True) # 外键为[指向]course_org
    # 课程名称
    name = models.CharField(max_length=50, verbose_name=u"课程名")
    desc = models.CharField(max_length=100, verbose_name=u"课程描述")
    detail = models.TextField(verbose_name=u"课程详情")   # 暂时命名textFiled  富文本

    # 设置区分轮播广告位 标志
    is_banner = models.BooleanField(default=False, verbose_name=u"是否轮播")

    # 添加该课程·讲师 外键 8-5 日期 4/4
    teacher = models.ForeignKey(Teacher, verbose_name=u"讲师", null=True, blank=True)
    degree = models.CharField(choices=(("cj","初级"), ("zj","中级"), ("gj","高级")), max_length=2, verbose_name=u"难度")
    learn_times = models.IntegerField(default=0, verbose_name=u"学习时长（分钟数）")
    # 课程 学习人数
    students = models.IntegerField(default=0, verbose_name=u"学习人数")
    fav_nums = models.IntegerField(default=0, verbose_name=u"收藏人数")
    image = models.ImageField(upload_to="courses/%Y/%m", verbose_name=u"封面图", max_length=100) # current_path+
    # 统计课程点击量[非开始学习]  可用于计算转化量
    click_nums = models.IntegerField(default=0, verbose_name=u"点击数")
    # 新添加 课程目录 4/2操作
    category = models.CharField(default=u"后端开发", max_length=20, verbose_name=u"课程类别")
    # 8-3添加 查询某一门课程的相关课程  或随机选一个
    tag = models.CharField(default="", verbose_name=u"课程标签", max_length=10, null=True, blank=True)
    # 课程须知  8-5 日期 4/4
    youneed_know = models.TextField(default="", max_length=300, verbose_name=u"课程须知", null=True, blank=True)
    # 老师告诉你能学到什么 8-5 日期 4/4
    teacher_tell = models.TextField(default="", max_length=300, verbose_name=u"老师告诉你能学到什么？", null=True, blank=True)
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    # Meta信息
    class Meta:
        verbose_name = u"课程"
        verbose_name_plural = verbose_name

    # 后期统计章节数 新添加 4/2
    def get_zj_nums(self):
        # 获取课程章节数
        # 当有外键指向course时 course可以反向取出该数据
        return self.lesson_set.all().count()   # 【queryset】返回所有的课程
    get_zj_nums.short_description = u"章节数"   # 该函数称为列的短描述

    # 点击 get_zj_nums之后 跳转
    # def go_to(self):
    #     from django.utils.safestring import mark_safe
    #     return mark_safe("<a href='http://www.baidu.com'>跳转</>")
    # go_to.short_description = u"跳转"   # 该函数称为列的短描述  若要显示 需添加到CourseAdmin vs1
    # 获取学习课程的人数【用于课程详情页】
    def get_learn_nums(self):
        return self.usercourse_set.all()[:6] # 最多取6个显示

    def get_course_lesson(self):
        # 获取课程所有章节
        return self.lesson_set.all()

    def __unicode__(self):
        return self.name


# 数据库的一对多 多对一 在Django中则是外键的映射


class Lesson(models.Model):
    '''
        课程model设计
    '''
    course = models.ForeignKey(Course, verbose_name=u"课程")
    name = models.CharField(max_length=100, verbose_name=u"章节名")
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"章节"
        verbose_name_plural = verbose_name

    def get_lesson_video(self):
        # 获取章节视频
        # 通过video指向lesson的外键 找到课程的video
        return self.video_set.all()

    def __unicode__(self):
        return self.name

class Video(models.Model):
    '''
        视频model设计
    '''
    # 指向lesson的外键
    lesson = models.ForeignKey(Lesson, verbose_name=u"章节")
    name = models.CharField(max_length=100, verbose_name=u"视频名称")
    learn_times = models.IntegerField(default=0, verbose_name=u"学习时长（分钟数）")
    url = models.CharField(default="", max_length=200, verbose_name=u"访问地址")
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"视频"
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.name

class CourseResource(models.Model):
    '''
        课程资源model设计
    '''
    course = models.ForeignKey(Course, verbose_name=u"课程")
    name = models.CharField(max_length=100, verbose_name=u"视频名称")
    download = models.FileField(upload_to="course/resource/%Y/%m", verbose_name=u"资源文件", max_length=100)  # 可以直接生成上传按钮
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"课程资源"
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.name