#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/4/6 16:43
# @Author  : maxinehehe
# @Site    : 
# @File    : mixin_utils.py
# @Software: PyCharm

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
'''
    调用装饰器 dispath函数名称不可变 login_required()自动验证了用户的状态 未登录自动调转到login页面
'''


class LoginRequiredMixin(object):
     @method_decorator(login_required(login_url='/login/'))
     def dispatch(self, request, *args, **kwargs):
         return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)