#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/12 15:42
# @Author  : maxinehehe
# @Site    : 
# @File    : adminx.py
# @Software: PyCharm
import xadmin
from .models import UserAsk, UserCourse, UserMessage, CourseComments, UserFavorite, UserProfile

class UserAskAdmin(object):
    # 更改显示字段
    list_display = ['name', 'mobile', 'course_name', 'add_time']
    # 添加搜索字段
    search_fields = ['name', 'mobile', 'course_name']   # 不用time进行搜索
    # 添加过滤器
    list_filter =['name', 'mobile', 'course_name', 'add_time']


class UserCourseAdmin(object):

    # 更改显示字段
    list_display = ['user', 'course', 'add_time']
    # 添加搜索字段
    search_fields = ['user', 'course']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['user', 'course', 'add_time']


class UserMessageAdmin(object):
    # 更改显示字段
    list_display = ['user', 'message', 'has_read', 'add_time']
    # 添加搜索字段
    search_fields = ['user', 'message', 'has_read']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['user', 'message', 'has_read', 'add_time']
    # 重写保存函数
    def save_models(self):
        obj = self.new_obj
        obj.save()

        if obj.user == 0:
            # 如果等于0 表示所有用户都会收到该信息
            all_users = UserProfile.objects.all()
            for user in all_users:
                user_mess = UserMessage()
                user_mess.user = user.id
                user_mess.message = obj.message
                user_mess.save()

            print obj.message

        # if obj.course_org is not none:
        #     course_org = obj.course_org
        #     course_org.course_nums = Courses.objects.filter(course_org=course_org).count()
        #     course_org.save()

class CourseCommentsAdmin(object):
     # 更改显示字段
    list_display = ['user', 'course', 'comments', 'add_time']
    # 添加搜索字段
    search_fields = ['user', 'course', 'comments']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['user', 'course', 'comments', 'add_time']



class UserFavoriteAdmin(object):

    # 更改显示字段
    list_display = ['user', 'fav_id', 'fav_type', 'add_time']
    # 添加搜索字段
    search_fields = ['user', 'fav_id', 'fav_type']  # 不用time进行搜索
    # 添加过滤器
    list_filter = ['user', 'fav_id', 'fav_type', 'add_time']




# 注册用户咨询
xadmin.site.register(UserAsk, UserAskAdmin)
# 注册用户课程
xadmin.site.register(UserCourse, UserCourseAdmin)
# 注册用户
xadmin.site.register(UserMessage, UserMessageAdmin)
# 注册用户收藏
xadmin.site.register(UserFavorite, UserFavoriteAdmin)
# 注册课程评论
xadmin.site.register(CourseComments, CourseCommentsAdmin)