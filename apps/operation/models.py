#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.db import models

from users.models import UserProfile
from courses.models import Course
# Create your models here.
'''
 设计表  属于上层设计 用于解决循环引用  也即是将其他表引用至此 进行综合处理他们之间的关系
'''


class UserAsk(models.Model):
    '''
    用户咨询【后期用于处理我要学习卷填】
    '''
    name = models.CharField(max_length=20, verbose_name=u"姓名")
    mobile = models.CharField(max_length=11, verbose_name=u"手机")
    course_name = models.CharField(max_length=50, verbose_name=u"课程名")
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"用户咨询"
        verbose_name_plural = verbose_name


class CourseComments(models.Model):
    '''
    用户课程评论 【右小框】
    '''
    user = models.ForeignKey(UserProfile, verbose_name=u"用户")  # 外键
    course = models.ForeignKey(Course, verbose_name=u"课程")
    comments = models.CharField(max_length=200, verbose_name=u"评论")
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"课程评论"
        verbose_name_plural = verbose_name


class UserFavorite(models.Model):
    '''
    用户收藏
    '''
    user = models.ForeignKey(UserProfile, verbose_name=u"用户")  # 外键
    # 收藏有三个种类
    # course = models.ForeignKey(Course, verbose_name=u"课程")
    fav_id = models.IntegerField(default=0, verbose_name=u"数据ID")
    fav_type = models.IntegerField(choices=((1,"课程"), (2,"课程机构"), (3,"讲师")), default=1, verbose_name=u"收藏类型")
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"用户收藏"
        verbose_name_plural = verbose_name




class UserMessage(models.Model):
    '''
    用户消息
    '''
    # 发给全员用户的消息 默认用户 登录用户 分成两种 Id and 0  加以区分  不为0即代表用户ID
    user = models.IntegerField(default=0, verbose_name=u"接收用户")
    message = models.CharField(max_length=500, verbose_name=u"消息内容")
    # 点击之后 是否读过
    has_read = models.BooleanField(default=False, verbose_name=u"是否已读")  # True 表示已读
    #  消息发送的时间 与 记录添加时间一致
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"用户消息"
        verbose_name_plural = verbose_name



class UserCourse(models.Model):
    '''
    用户学习的课程
    '''
    user = models.ForeignKey(UserProfile, verbose_name=u"用户")  # 外键

    course = models.ForeignKey(Course, verbose_name=u"课程")
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
        verbose_name = u"用户课程"
        verbose_name_plural = verbose_name

