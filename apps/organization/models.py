#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime
# Create your models here.
'''
 organization 所需的数据库
'''


class CityDict(models.Model):
    name = models.CharField(max_length=20, verbose_name=u"城市")
    desc = models.CharField(max_length=200, verbose_name=u"描述")
    add_time = models.DateTimeField(default=datetime.now)

    class Meta:
        verbose_name = u"城市"
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.name


class CourseOrg(models.Model):
     name = models.CharField(max_length=50, verbose_name=u"机构名称")
     desc = models.TextField(verbose_name=u"机构描述")

     # 添加于 2018/4/13
     tag = models.CharField(max_length=10, verbose_name=u"机构标签", default=u"全国知名")
     # 后期添加 机构类别
     category = models.TextField(default="pxjg",verbose_name=u"机构类别", max_length=20, choices=(("pxjg","培训机构"), ("gx", "高校"),("gr", "个人")))
     click_nums = models.IntegerField(default=0, verbose_name=u"点击数")
     fav_nums = models.IntegerField(default=0, verbose_name=u"收藏数")
     image = models.ImageField(upload_to="org/%Y/%m", verbose_name=u"logo", max_length=100)
     address = models.CharField(max_length=150, verbose_name=u"机构地址")
     city = models.ForeignKey(CityDict, verbose_name=u"所在城市")
     # 后期添加
     students = models.IntegerField(default=0, verbose_name=u"学习人数")
     course_nums = models.IntegerField(default=0, verbose_name=u"课程数")

     add_time = models.DateTimeField(verbose_name=u"添加时间", default=datetime.now)

     class Meta:
         verbose_name = u"课程机构"
         verbose_name_plural = verbose_name

     def get_teacher_nums(self):
         # 获取该机构的教师数量
         return self.teacher_set.all().count()

     def __unicode__(self):
        return self.name


class Teacher(models.Model):
    org = models.ForeignKey(CourseOrg, verbose_name=u"所属机构")

    name = models.CharField(max_length=50, verbose_name=u"教师名称")
    work_years = models.IntegerField(default=0, verbose_name=u"工作年限")
    work_company = models.CharField(max_length=50, verbose_name=u"就职公司")
    work_position = models.CharField(max_length=50, verbose_name=u"公司职位")
    points = models.CharField(max_length=50, verbose_name=u"教学特点")
    click_nums = models.IntegerField(default=0, verbose_name=u"点击数")
    fav_nums = models.IntegerField(default=0, verbose_name=u"收藏数")
    # 添加年龄 2018/4/7
    age = models.IntegerField(default=18, verbose_name=u"年龄")
    image = models.ImageField(default='', upload_to="teacher/%Y/%m", verbose_name=u"讲师头像", max_length=100)
    add_time = models.DateTimeField(default=datetime.now, verbose_name=u"添加时间")

    class Meta:
         verbose_name = u"教师"
         verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.name  # 添加成功后返回教师的姓名

    def get_course_nums(self):
        # 获取课程数 通过外键
        # 反向查询也可以。要查看一个讲师的所有课程,teacher(实例或self).course_set ,就如这样
        return self.course_set.all().count()
