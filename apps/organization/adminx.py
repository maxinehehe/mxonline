#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/12 15:30
# @Author  : maxinehehe
# @Site    : 
# @File    : adminx.py
# @Software: PyCharm
import xadmin
from .models import CityDict, CourseOrg, Teacher

class CityDictAdmin(object):
    # 更改显示字段
    list_display = ['name', 'desc', 'add_time']
    # 添加搜索字段
    search_fields = ['name', 'desc']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['name', 'desc', 'add_time']

class CourseOrgAdmin(object):
    # 更改显示字段
    list_display = ['name',  'click_nums', 'fav_nums', 'image', 'address']
    # 添加搜索字段
    search_fields = ['name',  'click_nums', 'fav_nums', 'image', 'address']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['name',  'click_nums', 'fav_nums', 'image', 'address']
    # 设置课程搜索 课程机构过多则以搜索代指
    # relfield_style = 'fk-ajax'   # fk：外键  暂时不可用


class TeacherAdmin(object):

    # 更改显示字段
    list_display = ['name','org',  'work_years', 'work_company', 'work_position', 'click_nums', 'fav_nums', 'add_time']
    # 添加搜索字段
    search_fields = [ 'name', 'org','work_years', 'work_company', 'work_position', 'click_nums', 'fav_nums']   # 不用time进行搜索
    # 添加过滤器
    list_filter = ['org', 'name', 'work_years', 'work_company', 'work_position', 'click_nums', 'fav_nums', 'add_time']


# 注册城市事件
xadmin.site.register(CityDict, CityDictAdmin)
# 注册课程机构事件
xadmin.site.register(CourseOrg, CourseOrgAdmin)
# 注册教师事件
xadmin.site.register(Teacher, TeacherAdmin)