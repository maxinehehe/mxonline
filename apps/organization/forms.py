#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/1/30 9:34
# @Author  : maxinehehe
# @Site    : 
# @File    : forms.py
# @Software: PyCharm

from django import forms
import re

from operation.models import UserAsk


# class UserAskForm(forms.Form):
#     name = forms.CharField(required=True, min_length=2, max_length=20)
#     phone = forms.CharField(required=True, min_length=11,max_length=11)
#     course_name = forms.CharField(required=True, min_length=5, max_length=50)
#     # model在相似的情况下可以转换为form
# 更改为下面的ModelForm


class UserAskForm(forms.ModelForm):
    # 甚至可以新增字段
    # my_field = forms.CharField()

    '''
    继承modelform去做 这样可以不用专门form  其save时 可以直接保存至数据库
    因此 ModelForm相对于From优势还是相当明显的
    '''
    # 定义meta信息
    class Meta():
        model = UserAsk
        # 可以选取只要某些字段 验证时默认不为空
        fields = ['name', 'mobile','course_name' ]

    # 直接在表单中进行验证
    # 定义函数 clean【不可变 指定】
    def clean_mobile(self):
        '''
        对手机号码进行验证
        :return:
        '''
        # 在初始化表单时 会自动调用该方法 对 fileds列表内字段进行验证
        # 取出 fileds 中的mobile
        mobile = self.cleaned_data['mobile']  # form内置变量 cleaned_data 字典类型
        # 正则表达式匹配 re模块
        regex_mobile = "^1[358]\d{9}$|^147\d{8}$|^176\d{8}$"
        p = re.compile(regex_mobile)
        if p.match(mobile):
            return mobile
        else:
            # 抛出自定义错误信息
            raise forms.ValidationError(u"手机号码不符合要求，请正确填写", code="mobile_invalid")