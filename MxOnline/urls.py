#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""MxOnline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic import TemplateView
import xadmin
from django.views.static import serve  # 处理静态文件

from users.views import LoginView, RegisterView, ActiveUserView, ForgetPwdView, ResetView, ModifyPwdView
from users.views import LogoutView, IndexView
from organization.views import OrgView
from MxOnline.settings import MEDIA_ROOT
# from MxOnline.settings import STATIC_ROOT

urlpatterns = [
    url(r'^xadmin/', xadmin.site.urls),
    # 利用Django本身自带TemplateView处理静态文件文件
    url('^$', IndexView.as_view(), name='index'),  # 调用此方法即可自动转换出view方法
    # 只需要指定index文件 # 这样就不需要写后台的viewDjango就会自动跳转到指定Template
    url('^login/$',LoginView.as_view(), name='login'),

    # 2018/4/12 配置 退出
    url('^logout/$',LogoutView.as_view(), name='logout'),
    # 只是传递句柄  指向这个函数并不是调用它
    url('^register/$', RegisterView.as_view(), name="register"),
    url(r'^captcha/', include('captcha.urls')), # 引入验证码view
    # 处理用户激活 提取传过来的url变量  采用python 分组贪婪匹配 放到active_code中

    # 链接失效 或者链接错误
    url(r'^active/(?P<active_code>.*)/$', ActiveUserView.as_view(), name="user_active"),

    url(r'^forget/$', ForgetPwdView.as_view(), name="forget_pwd"),
    # 新的接口 处理重设密码
    url(r'^reset/(?P<active_code>.*)/$', ResetView.as_view(), name="reset_pwd"),
    # 重置密码
    url(r'^modify_pwd/$', ModifyPwdView.as_view(), name="modify_pwd"),

    # 课程机构的url配置 namespace
    # org/作为 organization.urls 路由中的总 路由开头
    url(r'^org/', include('organization.urls', namespace="org")), #使用二级路由 即url的分发

    # 公开课 课程相关url的配置
    url(r'^course/', include('courses.urls', namespace="course")), #使用二级路由 即url的分发


    # 处理media 信息 读取课程机构图片信息【配置上传文件的访问处理函数】
    url(r'^media/(?P<path>.*)$', serve, {'document_root':MEDIA_ROOT}),

    # 解决DEBUG为False的情况 Django不再为系统自行代理  需要Apache之类的代理 可以自行编写处理
    # 处理media 信息 读取课程机构图片信息【配置上传文件的访问处理函数】
    # url(r'^static/(?P<path>.*)$', serve, {'document_root':STATIC_ROOT}),


    # 用户相关url的配置 跳转到 用户app下
    url(r'^users/', include('users.urls', namespace="users")), #使用二级路由 即url的分发


]

# 全局404页面相关配置
handler404 = 'users.views.page_not_found'

# 全局500页面配置
handler500 = 'users.views.page_error'