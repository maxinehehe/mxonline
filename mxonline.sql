/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : mxonline

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-04-06 14:04:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_group_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_permission`
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('4', 'Can add permission', '2', 'add_permission');
INSERT INTO `auth_permission` VALUES ('5', 'Can change permission', '2', 'change_permission');
INSERT INTO `auth_permission` VALUES ('6', 'Can delete permission', '2', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('7', 'Can add group', '3', 'add_group');
INSERT INTO `auth_permission` VALUES ('8', 'Can change group', '3', 'change_group');
INSERT INTO `auth_permission` VALUES ('9', 'Can delete group', '3', 'delete_group');
INSERT INTO `auth_permission` VALUES ('13', 'Can add content type', '5', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('14', 'Can change content type', '5', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('16', 'Can add session', '6', 'add_session');
INSERT INTO `auth_permission` VALUES ('17', 'Can change session', '6', 'change_session');
INSERT INTO `auth_permission` VALUES ('18', 'Can delete session', '6', 'delete_session');
INSERT INTO `auth_permission` VALUES ('19', 'Can add 用户信息', '7', 'add_userprofile');
INSERT INTO `auth_permission` VALUES ('20', 'Can change 用户信息', '7', 'change_userprofile');
INSERT INTO `auth_permission` VALUES ('21', 'Can delete 用户信息', '7', 'delete_userprofile');
INSERT INTO `auth_permission` VALUES ('22', 'Can add 邮箱验证码', '8', 'add_emailverifyrecord');
INSERT INTO `auth_permission` VALUES ('23', 'Can change 邮箱验证码', '8', 'change_emailverifyrecord');
INSERT INTO `auth_permission` VALUES ('24', 'Can delete 邮箱验证码', '8', 'delete_emailverifyrecord');
INSERT INTO `auth_permission` VALUES ('25', 'Can add 轮播图', '9', 'add_banner');
INSERT INTO `auth_permission` VALUES ('26', 'Can change 轮播图', '9', 'change_banner');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete 轮播图', '9', 'delete_banner');
INSERT INTO `auth_permission` VALUES ('28', 'Can add 课程', '10', 'add_course');
INSERT INTO `auth_permission` VALUES ('29', 'Can change 课程', '10', 'change_course');
INSERT INTO `auth_permission` VALUES ('30', 'Can delete 课程', '10', 'delete_course');
INSERT INTO `auth_permission` VALUES ('31', 'Can add 章节', '11', 'add_lesson');
INSERT INTO `auth_permission` VALUES ('32', 'Can change 章节', '11', 'change_lesson');
INSERT INTO `auth_permission` VALUES ('33', 'Can delete 章节', '11', 'delete_lesson');
INSERT INTO `auth_permission` VALUES ('34', 'Can add 视频', '12', 'add_video');
INSERT INTO `auth_permission` VALUES ('35', 'Can change 视频', '12', 'change_video');
INSERT INTO `auth_permission` VALUES ('36', 'Can delete 视频', '12', 'delete_video');
INSERT INTO `auth_permission` VALUES ('37', 'Can add 课程资源', '13', 'add_courseresource');
INSERT INTO `auth_permission` VALUES ('38', 'Can change 课程资源', '13', 'change_courseresource');
INSERT INTO `auth_permission` VALUES ('39', 'Can delete 课程资源', '13', 'delete_courseresource');
INSERT INTO `auth_permission` VALUES ('40', 'Can add 城市', '14', 'add_citydict');
INSERT INTO `auth_permission` VALUES ('41', 'Can change 城市', '14', 'change_citydict');
INSERT INTO `auth_permission` VALUES ('42', 'Can delete 城市', '14', 'delete_citydict');
INSERT INTO `auth_permission` VALUES ('43', 'Can add 课程机构', '15', 'add_courseorg');
INSERT INTO `auth_permission` VALUES ('44', 'Can change 课程机构', '15', 'change_courseorg');
INSERT INTO `auth_permission` VALUES ('45', 'Can delete 课程机构', '15', 'delete_courseorg');
INSERT INTO `auth_permission` VALUES ('46', 'Can add 教师', '16', 'add_teacher');
INSERT INTO `auth_permission` VALUES ('47', 'Can change 教师', '16', 'change_teacher');
INSERT INTO `auth_permission` VALUES ('48', 'Can delete 教师', '16', 'delete_teacher');
INSERT INTO `auth_permission` VALUES ('49', 'Can add 用户咨询', '17', 'add_userask');
INSERT INTO `auth_permission` VALUES ('50', 'Can change 用户咨询', '17', 'change_userask');
INSERT INTO `auth_permission` VALUES ('51', 'Can delete 用户咨询', '17', 'delete_userask');
INSERT INTO `auth_permission` VALUES ('52', 'Can add 课程评论', '18', 'add_coursecomments');
INSERT INTO `auth_permission` VALUES ('53', 'Can change 课程评论', '18', 'change_coursecomments');
INSERT INTO `auth_permission` VALUES ('54', 'Can delete 课程评论', '18', 'delete_coursecomments');
INSERT INTO `auth_permission` VALUES ('55', 'Can add 用户收藏', '19', 'add_userfavorite');
INSERT INTO `auth_permission` VALUES ('56', 'Can change 用户收藏', '19', 'change_userfavorite');
INSERT INTO `auth_permission` VALUES ('57', 'Can delete 用户收藏', '19', 'delete_userfavorite');
INSERT INTO `auth_permission` VALUES ('58', 'Can add 用户消息', '20', 'add_usermessage');
INSERT INTO `auth_permission` VALUES ('59', 'Can change 用户消息', '20', 'change_usermessage');
INSERT INTO `auth_permission` VALUES ('60', 'Can delete 用户消息', '20', 'delete_usermessage');
INSERT INTO `auth_permission` VALUES ('61', 'Can add 用户课程', '21', 'add_usercourse');
INSERT INTO `auth_permission` VALUES ('62', 'Can change 用户课程', '21', 'change_usercourse');
INSERT INTO `auth_permission` VALUES ('63', 'Can delete 用户课程', '21', 'delete_usercourse');
INSERT INTO `auth_permission` VALUES ('64', 'Can view log entry', '1', 'view_logentry');
INSERT INTO `auth_permission` VALUES ('65', 'Can view group', '3', 'view_group');
INSERT INTO `auth_permission` VALUES ('66', 'Can view permission', '2', 'view_permission');
INSERT INTO `auth_permission` VALUES ('67', 'Can view content type', '5', 'view_contenttype');
INSERT INTO `auth_permission` VALUES ('68', 'Can view 课程', '10', 'view_course');
INSERT INTO `auth_permission` VALUES ('69', 'Can view 课程资源', '13', 'view_courseresource');
INSERT INTO `auth_permission` VALUES ('70', 'Can view 章节', '11', 'view_lesson');
INSERT INTO `auth_permission` VALUES ('71', 'Can view 视频', '12', 'view_video');
INSERT INTO `auth_permission` VALUES ('72', 'Can view 课程评论', '18', 'view_coursecomments');
INSERT INTO `auth_permission` VALUES ('73', 'Can view 用户咨询', '17', 'view_userask');
INSERT INTO `auth_permission` VALUES ('74', 'Can view 用户课程', '21', 'view_usercourse');
INSERT INTO `auth_permission` VALUES ('75', 'Can view 用户收藏', '19', 'view_userfavorite');
INSERT INTO `auth_permission` VALUES ('76', 'Can view 用户消息', '20', 'view_usermessage');
INSERT INTO `auth_permission` VALUES ('77', 'Can view 城市', '14', 'view_citydict');
INSERT INTO `auth_permission` VALUES ('78', 'Can view 课程机构', '15', 'view_courseorg');
INSERT INTO `auth_permission` VALUES ('79', 'Can view 教师', '16', 'view_teacher');
INSERT INTO `auth_permission` VALUES ('80', 'Can view session', '6', 'view_session');
INSERT INTO `auth_permission` VALUES ('81', 'Can view 轮播图', '9', 'view_banner');
INSERT INTO `auth_permission` VALUES ('82', 'Can view 邮箱验证码', '8', 'view_emailverifyrecord');
INSERT INTO `auth_permission` VALUES ('83', 'Can view 用户信息', '7', 'view_userprofile');
INSERT INTO `auth_permission` VALUES ('84', 'Can add Bookmark', '22', 'add_bookmark');
INSERT INTO `auth_permission` VALUES ('85', 'Can change Bookmark', '22', 'change_bookmark');
INSERT INTO `auth_permission` VALUES ('86', 'Can delete Bookmark', '22', 'delete_bookmark');
INSERT INTO `auth_permission` VALUES ('87', 'Can add User Setting', '23', 'add_usersettings');
INSERT INTO `auth_permission` VALUES ('88', 'Can change User Setting', '23', 'change_usersettings');
INSERT INTO `auth_permission` VALUES ('89', 'Can delete User Setting', '23', 'delete_usersettings');
INSERT INTO `auth_permission` VALUES ('90', 'Can add User Widget', '24', 'add_userwidget');
INSERT INTO `auth_permission` VALUES ('91', 'Can change User Widget', '24', 'change_userwidget');
INSERT INTO `auth_permission` VALUES ('92', 'Can delete User Widget', '24', 'delete_userwidget');
INSERT INTO `auth_permission` VALUES ('93', 'Can view Bookmark', '22', 'view_bookmark');
INSERT INTO `auth_permission` VALUES ('94', 'Can view User Setting', '23', 'view_usersettings');
INSERT INTO `auth_permission` VALUES ('95', 'Can view User Widget', '24', 'view_userwidget');
INSERT INTO `auth_permission` VALUES ('96', 'Can add log entry', '25', 'add_log');
INSERT INTO `auth_permission` VALUES ('97', 'Can change log entry', '25', 'change_log');
INSERT INTO `auth_permission` VALUES ('98', 'Can delete log entry', '25', 'delete_log');
INSERT INTO `auth_permission` VALUES ('99', 'Can view log entry', '25', 'view_log');
INSERT INTO `auth_permission` VALUES ('100', 'Can add captcha store', '26', 'add_captchastore');
INSERT INTO `auth_permission` VALUES ('101', 'Can change captcha store', '26', 'change_captchastore');
INSERT INTO `auth_permission` VALUES ('102', 'Can delete captcha store', '26', 'delete_captchastore');
INSERT INTO `auth_permission` VALUES ('103', 'Can view captcha store', '26', 'view_captchastore');

-- ----------------------------
-- Table structure for `auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for `auth_user_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `captcha_captchastore`
-- ----------------------------
DROP TABLE IF EXISTS `captcha_captchastore`;
CREATE TABLE `captcha_captchastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge` varchar(32) NOT NULL,
  `response` varchar(32) NOT NULL,
  `hashkey` varchar(40) NOT NULL,
  `expiration` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashkey` (`hashkey`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of captcha_captchastore
-- ----------------------------
INSERT INTO `captcha_captchastore` VALUES ('226', 'BYIM', 'byim', '4eab1cedd674a4c192d0bf8dd228af0346ce383a', '2018-03-29 12:06:57.741000');
INSERT INTO `captcha_captchastore` VALUES ('227', 'MBXM', 'mbxm', '13459dbeeeffd0616cb997bf890896ea6b6af458', '2018-03-29 12:07:34.193000');
INSERT INTO `captcha_captchastore` VALUES ('228', 'BUZQ', 'buzq', 'f01cacf7059f2fdda82ebde760f8e70458c15783', '2018-03-29 12:08:20.257000');
INSERT INTO `captcha_captchastore` VALUES ('229', 'NOLS', 'nols', '2a3906b83307bd21d1d337dd36c043e2ced599ee', '2018-03-29 12:08:39.912000');
INSERT INTO `captcha_captchastore` VALUES ('231', 'UWIN', 'uwin', '2d7faf10c7fcd1a7f126f93290ca0ddfbeeb6376', '2018-03-29 12:08:47.402000');

-- ----------------------------
-- Table structure for `courses_course`
-- ----------------------------
DROP TABLE IF EXISTS `courses_course`;
CREATE TABLE `courses_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `detail` longtext NOT NULL,
  `degree` varchar(2) NOT NULL,
  `learn_times` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `fav_nums` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `click_nums` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_org_id` int(11),
  `category` varchar(20) NOT NULL,
  `tag` varchar(10) NOT NULL,
  `teacher_id` int(11),
  `teacher_tell` longtext,
  `youneed_know` longtext,
  PRIMARY KEY (`id`),
  KEY `courses_course_11456c5a` (`course_org_id`),
  KEY `courses_course_d9614d40` (`teacher_id`),
  CONSTRAINT `courses_cour_course_org_id_4d2c4aab_fk_organization_courseorg_id` FOREIGN KEY (`course_org_id`) REFERENCES `organization_courseorg` (`id`),
  CONSTRAINT `courses_course_teacher_id_846fa526_fk_organization_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `organization_teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of courses_course
-- ----------------------------
INSERT INTO `courses_course` VALUES ('1', 'Django入门', 'Django入门', 'Django入门', 'cj', '0', '0', '0', 'courses/2018/02/mysql.jpg', '44', '2018-01-12 15:17:00.000000', '1', '后端开发', 'python', '1', '1、搭建完整的Django开发环境。 \r\n2、创建项目及应用。 \r\n3、了解项目目录下各文件的含义和作用。 \r\n4、了解并学会开发Templates。 \r\n5、了解并学会开发Models。 \r\n6、掌握Admin的基本配置方法。 \r\n7、学会项目URL的配置方法。 \r\n8、开发一个由三个页面组成的简易博客网站。', '1、了解HTML的用法。 \r\n2、基本掌握Python的用法。');
INSERT INTO `courses_course` VALUES ('4', 'java', 'java学习', '无', 'cj', '20', '0', '0', 'courses/2018/02/57035ff200014b8a06000338-240-135_dHfj8Nq.jpg', '1', '2018-02-02 17:54:00.000000', '2', '后端开发', '', null, '', '');
INSERT INTO `courses_course` VALUES ('5', 'go语言入门', '实战项目', '用于快速进入学习', 'cj', '23', '0', '0', 'courses/2018/03/1截图桌面.PNG', '2', '2018-03-29 15:14:00.000000', '3', '后端开发', '', null, '', '');
INSERT INTO `courses_course` VALUES ('6', 'java中级', '提升java知识能力，进阶', '进一步深入理解java，提升职场工作能力', 'zj', '0', '0', '0', 'courses/2018/03/134903993532.jpg', '4', '2018-03-29 15:19:00.000000', '9', '后端开发', '', null, '', '');
INSERT INTO `courses_course` VALUES ('7', 'python入门', '基础python', '用于了解python基本的语法知识，并进一步提升自身python实力', 'cj', '0', '0', '0', 'courses/2018/03/13489863817.jpg', '25', '2018-03-29 15:21:00.000000', '1', '后端开发', 'python', null, '', '');
INSERT INTO `courses_course` VALUES ('8', '大数据分析', '处理数据', '进一步了解数据 进一步与数据进行交互', 'cj', '0', '0', '0', 'courses/2018/03/1348626500448.jpg', '1', '2018-03-29 16:23:00.000000', '3', '后端开发', '', null, '', '');
INSERT INTO `courses_course` VALUES ('9', '机器学习', '算法集合', '通过对算法的学习掌握最新的算法 如k-means', 'cj', '0', '0', '0', 'courses/2018/03/1348626485299.jpg', '3', '2018-03-29 16:24:00.000000', '1', '后端开发', '', null, '', '');
INSERT INTO `courses_course` VALUES ('10', '数据挖掘', '对数据进行清洗', '从新的层面 了解数据挖掘', 'zj', '0', '0', '0', 'courses/2018/03/1348985881890.jpg', '7', '2018-03-29 16:25:00.000000', '2', '后端开发', '', null, '', '');

-- ----------------------------
-- Table structure for `courses_courseresource`
-- ----------------------------
DROP TABLE IF EXISTS `courses_courseresource`;
CREATE TABLE `courses_courseresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `download` varchar(100) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_courseresource_course_id_5eba1332_fk_courses_course_id` (`course_id`),
  CONSTRAINT `courses_courseresource_course_id_5eba1332_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of courses_courseresource
-- ----------------------------
INSERT INTO `courses_courseresource` VALUES ('1', '前端页面', 'course/resource/2018/04/08165849rme3.zip', '2018-04-04 07:43:00.000000', '1');

-- ----------------------------
-- Table structure for `courses_lesson`
-- ----------------------------
DROP TABLE IF EXISTS `courses_lesson`;
CREATE TABLE `courses_lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_lesson_course_id_16bc4882_fk_courses_course_id` (`course_id`),
  CONSTRAINT `courses_lesson_course_id_16bc4882_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of courses_lesson
-- ----------------------------
INSERT INTO `courses_lesson` VALUES ('2', '第一章 基础知识', '2018-04-04 03:38:00.000000', '1');
INSERT INTO `courses_lesson` VALUES ('3', '第二章 进阶开发', '2018-04-04 03:39:00.000000', '1');

-- ----------------------------
-- Table structure for `courses_video`
-- ----------------------------
DROP TABLE IF EXISTS `courses_video`;
CREATE TABLE `courses_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `learn_times` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_video_lesson_id_59f2396e_fk_courses_lesson_id` (`lesson_id`),
  CONSTRAINT `courses_video_lesson_id_59f2396e_fk_courses_lesson_id` FOREIGN KEY (`lesson_id`) REFERENCES `courses_lesson` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of courses_video
-- ----------------------------
INSERT INTO `courses_video` VALUES ('1', '1.1 helloworld', '2018-04-04 03:43:00.000000', '2', 'http://www.immoc.com/video/1430', '0');
INSERT INTO `courses_video` VALUES ('2', '1.2 基本概念', '2018-04-04 03:46:00.000000', '2', 'http://www.immoc.com/video/1430', '0');
INSERT INTO `courses_video` VALUES ('3', '2.1 test', '2018-04-04 03:47:00.000000', '3', 'http://www.immoc.com/video/1430', '0');
INSERT INTO `courses_video` VALUES ('4', '2.2 test2', '2018-04-04 03:47:00.000000', '3', 'http://www.immoc.com/video/1430', '0');
INSERT INTO `courses_video` VALUES ('5', '1.3 Django高级应用之ORM', '2018-04-04 07:51:00.000000', '2', 'https://www.imooc.com/video/13931', '12');

-- ----------------------------
-- Table structure for `django_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO `django_admin_log` VALUES ('2', '2018-01-12 13:34:34.197000', '3', 'hehehehe', '1', '已添加。', '7', '1');

-- ----------------------------
-- Table structure for `django_content_type`
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('3', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('2', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('26', 'captcha', 'captchastore');
INSERT INTO `django_content_type` VALUES ('5', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('10', 'courses', 'course');
INSERT INTO `django_content_type` VALUES ('13', 'courses', 'courseresource');
INSERT INTO `django_content_type` VALUES ('11', 'courses', 'lesson');
INSERT INTO `django_content_type` VALUES ('12', 'courses', 'video');
INSERT INTO `django_content_type` VALUES ('18', 'operation', 'coursecomments');
INSERT INTO `django_content_type` VALUES ('17', 'operation', 'userask');
INSERT INTO `django_content_type` VALUES ('21', 'operation', 'usercourse');
INSERT INTO `django_content_type` VALUES ('19', 'operation', 'userfavorite');
INSERT INTO `django_content_type` VALUES ('20', 'operation', 'usermessage');
INSERT INTO `django_content_type` VALUES ('14', 'organization', 'citydict');
INSERT INTO `django_content_type` VALUES ('15', 'organization', 'courseorg');
INSERT INTO `django_content_type` VALUES ('16', 'organization', 'teacher');
INSERT INTO `django_content_type` VALUES ('6', 'sessions', 'session');
INSERT INTO `django_content_type` VALUES ('9', 'users', 'banner');
INSERT INTO `django_content_type` VALUES ('8', 'users', 'emailverifyrecord');
INSERT INTO `django_content_type` VALUES ('7', 'users', 'userprofile');
INSERT INTO `django_content_type` VALUES ('22', 'xadmin', 'bookmark');
INSERT INTO `django_content_type` VALUES ('25', 'xadmin', 'log');
INSERT INTO `django_content_type` VALUES ('23', 'xadmin', 'usersettings');
INSERT INTO `django_content_type` VALUES ('24', 'xadmin', 'userwidget');

-- ----------------------------
-- Table structure for `django_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES ('1', 'contenttypes', '0001_initial', '2018-01-11 02:49:01.679000');
INSERT INTO `django_migrations` VALUES ('2', 'auth', '0001_initial', '2018-01-11 02:49:10.238000');
INSERT INTO `django_migrations` VALUES ('3', 'admin', '0001_initial', '2018-01-11 02:49:12.179000');
INSERT INTO `django_migrations` VALUES ('4', 'admin', '0002_logentry_remove_auto_add', '2018-01-11 02:49:12.334000');
INSERT INTO `django_migrations` VALUES ('5', 'contenttypes', '0002_remove_content_type_name', '2018-01-11 02:49:13.436000');
INSERT INTO `django_migrations` VALUES ('6', 'auth', '0002_alter_permission_name_max_length', '2018-01-11 02:49:14.182000');
INSERT INTO `django_migrations` VALUES ('7', 'auth', '0003_alter_user_email_max_length', '2018-01-11 02:49:15.147000');
INSERT INTO `django_migrations` VALUES ('8', 'auth', '0004_alter_user_username_opts', '2018-01-11 02:49:15.227000');
INSERT INTO `django_migrations` VALUES ('9', 'auth', '0005_alter_user_last_login_null', '2018-01-11 02:49:16.133000');
INSERT INTO `django_migrations` VALUES ('10', 'auth', '0006_require_contenttypes_0002', '2018-01-11 02:49:16.212000');
INSERT INTO `django_migrations` VALUES ('11', 'auth', '0007_alter_validators_add_error_messages', '2018-01-11 02:49:16.273000');
INSERT INTO `django_migrations` VALUES ('12', 'sessions', '0001_initial', '2018-01-11 02:49:16.865000');
INSERT INTO `django_migrations` VALUES ('13', 'users', '0001_initial', '2018-01-11 03:27:13.498000');
INSERT INTO `django_migrations` VALUES ('14', 'courses', '0001_initial', '2018-01-12 05:07:12.746000');
INSERT INTO `django_migrations` VALUES ('15', 'operation', '0001_initial', '2018-01-12 05:07:18.288000');
INSERT INTO `django_migrations` VALUES ('16', 'organization', '0001_initial', '2018-01-12 05:07:20.396000');
INSERT INTO `django_migrations` VALUES ('17', 'users', '0002_banner_emailverifyrecord', '2018-01-12 05:07:21.163000');
INSERT INTO `django_migrations` VALUES ('18', 'users', '0003_auto_20180112_1306', '2018-01-12 05:07:21.419000');
INSERT INTO `django_migrations` VALUES ('19', 'xadmin', '0001_initial', '2018-01-12 14:03:30.773000');
INSERT INTO `django_migrations` VALUES ('20', 'xadmin', '0002_log', '2018-01-12 14:37:54.581000');
INSERT INTO `django_migrations` VALUES ('21', 'xadmin', '0003_auto_20160715_0100', '2018-01-12 14:37:55.398000');
INSERT INTO `django_migrations` VALUES ('22', 'courses', '0002_auto_20180112_1503', '2018-01-12 15:03:50.450000');
INSERT INTO `django_migrations` VALUES ('23', 'users', '0004_auto_20180112_1503', '2018-01-12 15:03:50.739000');
INSERT INTO `django_migrations` VALUES ('24', 'users', '0005_auto_20180112_1509', '2018-01-12 15:09:58.299000');
INSERT INTO `django_migrations` VALUES ('25', 'organization', '0002_auto_20180112_1536', '2018-01-12 15:36:42.491000');
INSERT INTO `django_migrations` VALUES ('26', 'organization', '0003_auto_20180112_1541', '2018-01-12 15:41:21.952000');
INSERT INTO `django_migrations` VALUES ('27', 'courses', '0003_auto_20180112_1812', '2018-01-12 18:12:06.136000');
INSERT INTO `django_migrations` VALUES ('28', 'courses', '0004_auto_20180114_0004', '2018-01-14 00:04:46.154000');
INSERT INTO `django_migrations` VALUES ('29', 'captcha', '0001_initial', '2018-01-16 21:45:25.853000');
INSERT INTO `django_migrations` VALUES ('30', 'organization', '0004_auto_20180123_1453', '2018-01-23 14:53:21.596000');
INSERT INTO `django_migrations` VALUES ('31', 'organization', '0005_auto_20180129_1816', '2018-01-29 18:16:40.424000');
INSERT INTO `django_migrations` VALUES ('32', 'organization', '0006_auto_20180202_1615', '2018-02-02 16:15:49.272000');
INSERT INTO `django_migrations` VALUES ('33', 'courses', '0005_course_course_org', '2018-02-02 16:15:52.231000');
INSERT INTO `django_migrations` VALUES ('34', 'organization', '0007_teacher_image', '2018-02-02 18:44:52.924000');
INSERT INTO `django_migrations` VALUES ('35', 'operation', '0002_remove_userfavorite_course', '2018-02-14 18:38:55.340000');
INSERT INTO `django_migrations` VALUES ('36', 'courses', '0006_auto_20180329_1518', '2018-03-29 15:18:57.195000');
INSERT INTO `django_migrations` VALUES ('37', 'courses', '0007_course_category', '2018-04-02 10:32:09.540000');
INSERT INTO `django_migrations` VALUES ('38', 'courses', '0008_course_tag', '2018-04-04 02:35:13.718000');
INSERT INTO `django_migrations` VALUES ('39', 'courses', '0009_video_url', '2018-04-04 03:43:28.218000');
INSERT INTO `django_migrations` VALUES ('40', 'courses', '0010_video_learn_times', '2018-04-04 07:40:49.513000');
INSERT INTO `django_migrations` VALUES ('41', 'courses', '0011_course_teacher', '2018-04-04 08:13:21.362000');
INSERT INTO `django_migrations` VALUES ('42', 'courses', '0012_auto_20180404_0826', '2018-04-04 08:26:24.613000');
INSERT INTO `django_migrations` VALUES ('43', 'courses', '0013_auto_20180404_0828', '2018-04-04 08:29:09.072000');
INSERT INTO `django_migrations` VALUES ('44', 'courses', '0014_auto_20180404_0831', '2018-04-04 08:32:07.375000');

-- ----------------------------
-- Table structure for `django_session`
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('90r5rl4e9mk36ec0n899lwe6puhiyrj1', 'ZTE2MTAyYzNiNzIwYjBmNmJiZjIyNzE0NTU3YWEwZWFlYjVmMGE3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlYzNhOWIzYTM3YmJhOTQ4M2I5MTQwMmVmNjQ4MDQ1ZWVhNWI3ZWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJ1c2Vycy52aWV3cy5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==', '2018-01-29 12:37:28.854000');
INSERT INTO `django_session` VALUES ('dizi12auxfwub0zgvxptgj8161yd0uoo', 'MWJmYjVmZWY1NjhjMTM0ZTRjYjllYjNlYTA1ZjIyMTU4YjI5NzgwYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InVzZXJzLnZpZXdzLkN1c3RvbUJhY2tlbmQiLCJMSVNUX1FVRVJZIjpbWyJ1c2VycyIsInVzZXJwcm9maWxlIl0sIiJdLCJfYXV0aF91c2VyX2hhc2giOiIxZWMzYTliM2EzN2JiYTk0ODNiOTE0MDJlZjY0ODA0NWVlYTViN2VjIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==', '2018-04-18 09:50:38.440000');
INSERT INTO `django_session` VALUES ('fy4b1st1brubimmbknr7wcxfc0k0u4li', 'NTkyM2NlNWM4OTdiOGI4ZWQxMzllZDUwMTljYTg1Mzk1MDUxZGY2YTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiTElTVF9RVUVSWSI6W1siYXV0aCIsImdyb3VwIl0sIiJdLCJfYXV0aF91c2VyX2hhc2giOiIxZWMzYTliM2EzN2JiYTk0ODNiOTE0MDJlZjY0ODA0NWVlYTViN2VjIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoidXNlcnMudmlld3MuQ3VzdG9tQmFja2VuZCJ9', '2018-03-21 13:36:32.596000');
INSERT INTO `django_session` VALUES ('h13ohijui829w9hlq842afgdjsn7q5i7', 'NGEyNTA2YTJmNjdiMWRkMjNjYmJhNmY5OGU0MjMxNGYwYmJlZTI1Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InVzZXJzLnZpZXdzLkN1c3RvbUJhY2tlbmQiLCJMSVNUX1FVRVJZIjpbWyJ1c2VycyIsImJhbm5lciJdLCIiXSwiX2F1dGhfdXNlcl9oYXNoIjoiMWVjM2E5YjNhMzdiYmE5NDgzYjkxNDAyZWY2NDgwNDVlZWE1YjdlYyIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-03-20 21:22:00.386000');
INSERT INTO `django_session` VALUES ('kiv9y9bnfjxgu0qunus0yn6ogv8pc0cz', 'ODg3ODZmZWQyOGJkZGY0ZjgwYzY5ZTcwZjM5NTc3ZTEwYTZkZDhiMDp7Il9hdXRoX3VzZXJfaGFzaCI6IjhlYjBlODQ3YWRjN2JlNTE4ZGJjNGQ1ODM4YmM5YTg5OThiYjg0YTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJ1c2Vycy52aWV3cy5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjcifQ==', '2018-01-31 12:23:31.924000');
INSERT INTO `django_session` VALUES ('kpado19u6oznlcmhauz0eu39y1goqcnp', 'NzMyNzJmMzNjNDljNTgzZTU1ZDMwYWQwMTJkM2EwNjcyNmY3NmQ5ODp7Il9hdXRoX3VzZXJfaGFzaCI6ImM3NWM2M2FiODNmNGYwZWZiZjBhMjNiN2ZhMjE1MDI5ZTQ0Njg4NTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJ1c2Vycy52aWV3cy5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEwIn0=', '2018-02-04 14:29:59.342000');
INSERT INTO `django_session` VALUES ('nz05dw38ovb055f0ielntf75aapmpir9', 'ZTE2MTAyYzNiNzIwYjBmNmJiZjIyNzE0NTU3YWEwZWFlYjVmMGE3OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlYzNhOWIzYTM3YmJhOTQ4M2I5MTQwMmVmNjQ4MDQ1ZWVhNWI3ZWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJ1c2Vycy52aWV3cy5DdXN0b21CYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==', '2018-02-28 18:29:39.272000');
INSERT INTO `django_session` VALUES ('tjz5pjpojh7k6wbbjtp10mrk88os79ol', 'OWQ4ZTBiMjkzYTRjYThkOGEwMTgyNjQxODM3Y2MzMDgwOGI5ODE0MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6InVzZXJzLnZpZXdzLkN1c3RvbUJhY2tlbmQiLCJMSVNUX1FVRVJZIjpbWyJjb3Vyc2VzIiwibGVzc29uIl0sIiJdLCJfYXV0aF91c2VyX2hhc2giOiIxZWMzYTliM2EzN2JiYTk0ODNiOTE0MDJlZjY0ODA0NWVlYTViN2VjIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==', '2018-04-16 12:17:20.881000');
INSERT INTO `django_session` VALUES ('ug5pm5svckhpp1z6zyz3khv19ac2q0dh', 'MzUxYzkxYjQ2OWJmOTk1NWQxMzFjODFkYjNhNWEwY2ZiYmJkYTgwMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiTElTVF9RVUVSWSI6W1siY291cnNlcyIsImNvdXJzZSJdLCIiXSwiX2F1dGhfdXNlcl9oYXNoIjoiMWVjM2E5YjNhMzdiYmE5NDgzYjkxNDAyZWY2NDgwNDVlZWE1YjdlYyIsIl9hdXRoX3VzZXJfaWQiOiIxIiwid2l6YXJkX3hhZG1pbnVzZXJ3aWRnZXRfYWRtaW5fd2l6YXJkX2Zvcm1fcGx1Z2luIjp7InN0ZXBfZmlsZXMiOnt9LCJzdGVwIjoiV2lkZ2V0XHU3YzdiXHU1NzhiIiwiZXh0cmFfZGF0YSI6e30sInN0ZXBfZGF0YSI6e319fQ==', '2018-01-27 23:54:13.201000');

-- ----------------------------
-- Table structure for `operation_coursecomments`
-- ----------------------------
DROP TABLE IF EXISTS `operation_coursecomments`;
CREATE TABLE `operation_coursecomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comments` varchar(200) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_coursecomments_course_id_c88f1b6a_fk_courses_course_id` (`course_id`),
  KEY `operation_coursecomment_user_id_f5ff70b3_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `operation_coursecomment_user_id_f5ff70b3_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`),
  CONSTRAINT `operation_coursecomments_course_id_c88f1b6a_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation_coursecomments
-- ----------------------------
INSERT INTO `operation_coursecomments` VALUES ('1', '讲的还可以吧', '2018-04-04 09:31:52.413000', '1', '1');
INSERT INTO `operation_coursecomments` VALUES ('2', '课程内容还挺丰富的，能学到一些东西。', '2018-04-04 09:51:22.194000', '1', '1');

-- ----------------------------
-- Table structure for `operation_userask`
-- ----------------------------
DROP TABLE IF EXISTS `operation_userask`;
CREATE TABLE `operation_userask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `course_name` varchar(50) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation_userask
-- ----------------------------
INSERT INTO `operation_userask` VALUES ('61', '而繁荣认同的', 'd', 'df', '2018-02-01 11:46:16.158000');
INSERT INTO `operation_userask` VALUES ('62', '六和恶', 'sdDFDS', 'dfdsf', '2018-02-01 12:50:01.660000');
INSERT INTO `operation_userask` VALUES ('63', 'sdffsd', '15236862145', 'sdfsd', '2018-02-01 13:11:48.689000');
INSERT INTO `operation_userask` VALUES ('64', '刘贺贺', '13459174021', 'Java基础', '2018-02-04 13:00:58.718000');
INSERT INTO `operation_userask` VALUES ('65', '黎明', '13254678989', 'C语言基础', '2018-02-04 13:01:37.280000');

-- ----------------------------
-- Table structure for `operation_usercourse`
-- ----------------------------
DROP TABLE IF EXISTS `operation_usercourse`;
CREATE TABLE `operation_usercourse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_usercourse_course_id_9f1eab2e_fk_courses_course_id` (`course_id`),
  KEY `operation_usercourse_user_id_835fe81a_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `operation_usercourse_course_id_9f1eab2e_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`),
  CONSTRAINT `operation_usercourse_user_id_835fe81a_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation_usercourse
-- ----------------------------
INSERT INTO `operation_usercourse` VALUES ('1', '2018-04-02 12:03:00.000000', '1', '12');

-- ----------------------------
-- Table structure for `operation_userfavorite`
-- ----------------------------
DROP TABLE IF EXISTS `operation_userfavorite`;
CREATE TABLE `operation_userfavorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fav_id` int(11) NOT NULL,
  `fav_type` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_userfavorite_user_id_ad46a6af_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `operation_userfavorite_user_id_ad46a6af_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation_userfavorite
-- ----------------------------
INSERT INTO `operation_userfavorite` VALUES ('6', '1', '2', '2018-04-04 03:05:26.000000', '1');
INSERT INTO `operation_userfavorite` VALUES ('7', '7', '1', '2018-04-04 03:14:45.434000', '1');

-- ----------------------------
-- Table structure for `operation_usermessage`
-- ----------------------------
DROP TABLE IF EXISTS `operation_usermessage`;
CREATE TABLE `operation_usermessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `has_read` tinyint(1) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation_usermessage
-- ----------------------------

-- ----------------------------
-- Table structure for `organization_citydict`
-- ----------------------------
DROP TABLE IF EXISTS `organization_citydict`;
CREATE TABLE `organization_citydict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of organization_citydict
-- ----------------------------
INSERT INTO `organization_citydict` VALUES ('1', '北京市', '北京市', '2018-01-23 14:34:00.000000');
INSERT INTO `organization_citydict` VALUES ('2', '上海市', '上海市', '2018-01-23 14:34:00.000000');
INSERT INTO `organization_citydict` VALUES ('3', '广州市', '广州市', '2018-01-23 14:34:00.000000');
INSERT INTO `organization_citydict` VALUES ('4', '深圳市', '深圳市', '2018-01-23 14:35:00.000000');
INSERT INTO `organization_citydict` VALUES ('5', '天津市', '天津市', '2018-01-23 14:35:00.000000');
INSERT INTO `organization_citydict` VALUES ('6', '南京市', '南京市', '2018-01-23 15:00:00.000000');

-- ----------------------------
-- Table structure for `organization_courseorg`
-- ----------------------------
DROP TABLE IF EXISTS `organization_courseorg`;
CREATE TABLE `organization_courseorg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` longtext NOT NULL,
  `click_nums` int(11) NOT NULL,
  `fav_nums` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `address` varchar(150) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `city_id` int(11) NOT NULL,
  `category` longtext NOT NULL,
  `course_nums` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organization_course_city_id_4a842f85_fk_organization_citydict_id` (`city_id`),
  CONSTRAINT `organization_course_city_id_4a842f85_fk_organization_citydict_id` FOREIGN KEY (`city_id`) REFERENCES `organization_citydict` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of organization_courseorg
-- ----------------------------
INSERT INTO `organization_courseorg` VALUES ('1', '慕课网', '慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术\r\n牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n        慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。[1] \r\n        4月2日，国内首个IT技能学习类应用——慕课网3.1.0版本在应用宝首发。据了解，在此次上线的版本中，慕课网新增了课程历史记录、相关课程推荐等四大功能，为用户营造更加丰富的移动端IT学习体验。', '0', '0', 'org/2018/01/imooc.png', '北京市', '2018-01-23 14:36:00.000000', '1', 'pxjg', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('2', '清华大学', '清华大学（Tsinghua University），简称“清华”，由中华人民共和国教育部直属，中央直管副部级建制，位列“211工程”、“985工程”、“世界一流大学和一流学科”，入选“基础学科拔尖学生培养试验计划”、“高等学校创新能力提升计划”、“高等学校学科创新引智计划”，为九校联盟、中国大学校长联谊会、东亚研究型大学协会、亚洲大学联盟、环太平洋大学联盟、清华—剑桥—MIT低碳大学联盟成员，被誉为“红色工程师的摇篮”。\r\n清华大学的前身清华学堂始建于1911年，因水木清华而得名，是清政府设立的留美预备学校，其建校的资金源于1908年美国退还的部分庚子赔款。1912年更名为清华学校。1928年更名为国立清华大学。1937年抗日战争全面爆发后南迁长沙，与北京大学、南开大学组建国立长沙临时大学，1938年迁至昆明改名为国立西南联合大学。1946年迁回清华园。1949年中华人民共和国成立，清华大学进入了新的发展阶段。1952年全国高等学校院系调整后成为多科性工业大学。1978年以来逐步恢复和发展为综合性的研究型大学。\r\n水木清华，钟灵毓秀，清华大学秉持“自强不息、厚德载物”的校训和“行胜于言”的校风，坚持“中西融汇、古今贯通、文理渗透”的办学风格和“又红又专、全面发展”的培养特色，弘扬“爱国奉献、追求卓越”传统和“人文日新”精神。恰如清华园工字厅内对联所书——“槛外山光，历春夏秋冬、万千变幻，都非凡境；窗中云影，任东西南北、去来澹荡，洵是仙居”。', '0', '0', 'org/2018/01/qhdx-logo.png', '北京市', '2018-01-23 14:55:00.000000', '1', 'gx', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('3', '北京大学', '北京大学，简称“北大”，诞生于1898年，初名京师大学堂，是中国近代第一所国立大学，也是最早以“大学”之名创办的学校，其成立标志着中国近代高等教育的开端。北大是中国近代以来唯一以国家最高学府身份创立的学校，最初也是国家最高教育行政机关，行使教育部职能，统管全国教育。北大催生了中国最早的现代学制，开创了中国最早的文科、理科、社科、农科、医科等大学学科，是近代以来中国高等教育的奠基者。[1] \r\n1912年5月3日，京师大学堂改称北京大学校，严复为首任校长。[2]  1916年，蔡元培出任校长，“循思想自由原则、取兼容并包之义”，把北大办成全国学术和思想中心，使北大成为新文化运动中心、五四运动策源地。1937年抗日战争爆发，北大与清华大学、南开大学南迁长沙，组成国立长沙临时大学。不久迁往昆明，改称国立西南联合大学。1946年10月在北平复学。[1]  1952年院系调整，校园从内城沙滩红楼迁至西北郊燕园。[3-4] \r\n北大由教育部直属，中央直管副部级建制，是国家双一流[5]  、211工程[1]  、985工程[1]  、2011计划重点建设的全国重点大学；是九校联盟（C9）[6]  及中国大学校长联谊会、亚洲大学联盟[7]  、东亚研究型大学协会、国际研究型大学联盟、环太平洋大学联盟、东亚四大学论坛、国际公立大学论坛、中俄综合性大学联盟[8]  重要成员。[9-11] \r\n北大始终与国家民族的命运紧密相连，聚集了许多学者专家，培养了众多优秀人才，创造了大批重大科学成果，影响和推动了中国近现代思想理论、科学技术、文化教育和社会发展的进程。', '0', '0', 'org/2018/01/bjdx.jpg', '北京市', '2018-01-23 14:56:00.000000', '1', 'gx', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('4', '慕课网2', '慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。[1] \r\n4月2日，国内首个IT技能学习类应用——慕课网3.1.0版本在应用宝首发。据了解，在此次上线的版本中，慕课网新增了课程历史记录、相关课程推荐等四大功能，为用户营造更加丰富的移动端IT学习体验。', '0', '0', 'org/2018/01/imooc_Gn1sRjp.png', '上海市', '2018-01-23 14:57:00.000000', '2', 'pxjg', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('5', '慕课网3', '慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。[1] \r\n4月2日，国内首个IT技能学习类应用——慕课网3.1.0版本在应用宝首发。据了解，在此次上线的版本中，慕课网新增了课程历史记录、相关课程推荐等四大功能，为用户营造更加丰富的移动端IT学习体验。', '0', '0', 'org/2018/01/imooc_klgAUn5.png', '广州市', '2018-01-23 14:57:00.000000', '3', 'pxjg', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('6', '慕课网4', '慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。[1] \r\n4月2日，国内首个IT技能学习类应用——慕课网3.1.0版本在应用宝首发。据了解，在此次上线的版本中，慕课网新增了课程历史记录、相关课程推荐等四大功能，为用户营造更加丰富的移动端IT学习体验。', '0', '0', 'org/2018/01/imooc_OO2ykYP.png', '深圳市', '2018-01-23 14:58:00.000000', '4', 'gr', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('7', '慕课网5', '慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。[1] \r\n4月2日，国内首个IT技能学习类应用——慕课网3.1.0版本在应用宝首发。据了解，在此次上线的版本中，慕课网新增了课程历史记录、相关课程推荐等四大功能，为用户营造更加丰富的移动端IT学习体验。', '0', '0', 'org/2018/01/imooc_qEaMov1.png', '天津市', '2018-01-23 14:58:00.000000', '5', 'pxjg', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('8', '慕课网666', '慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。[1] \r\n4月2日，国内首个IT技能学习类应用——慕课网3.1.0版本在应用宝首发。据了解，在此次上线的版本中，慕课网新增了课程历史记录、相关课程推荐等四大功能，为用户营造更加丰富的移动端IT学习体验。', '0', '0', 'org/2018/01/imooc_Y2Tonsq.png', '北京市', '2018-01-23 14:59:00.000000', '1', 'pxjg', '0', '0');
INSERT INTO `organization_courseorg` VALUES ('9', '南京大学', '南京大学（Nanjing University），简称“南大”，是中华人民共和国教育部直属、中央直管副部级建制的全国重点大学，国家首批“双一流”、“211工程”、“985工程”重点建设高校，入选首批“珠峰计划”、“111计划”、“2011计划”、“卓越工程师教育培养计划”、“卓越医生教育培养计划”、“卓越法律人才教育培养计划”，九校联盟、中国大学校长联谊会、环太平洋大学联盟、21世纪学术联盟和东亚研究型大学协会成员。\r\n南京大学是中国第一所集教学和研究于一体的现代大学，其学脉可追溯自孙吴永安元年（258年）的南京太学，近代校史肇始于1902年筹办的三江师范学堂，此后历经多次变迁。1920年在中国国立高等学府中首开“女禁”，引领男女同校之风。最早在中国开展现代学术研究，建立中国最早的现代科学研究实验室，成为中国最早的以大学自治、学术自由、文理为基本兼有农工商等专门应用科、集教学和研究于一体为特征的现代大学。《学衡》月刊的创办，使得学校成为中西学术文化交流的中心，被誉为“东方教育的中心”。1949年由民国时期中国最高学府“国立中央大学”易名“国立南京大学”，翌年径称“南京大学”，沿用至今。\r\n南京大学是中国现代科学的发祥地之一，是哈佛大学白碧德主义影响下的中国“学衡派”的雅集地，被誉为“中国科学社的大本营和科学发展的主要基地”，是“985工程”首批九所高水平大学中唯一未合并其他院校的高校，坚持“内涵发展”，赢得社会“中国最温和的大学”之美誉。', '0', '0', 'org/2018/01/njdx.jpg', '南京市', '2018-01-23 15:01:00.000000', '6', 'gx', '0', '0');

-- ----------------------------
-- Table structure for `organization_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `organization_teacher`;
CREATE TABLE `organization_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `work_years` int(11) NOT NULL,
  `work_company` varchar(50) NOT NULL,
  `work_position` varchar(50) NOT NULL,
  `points` varchar(50) NOT NULL,
  `click_nums` int(11) NOT NULL,
  `fav_nums` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `org_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organization_teache_org_id_cd000a1a_fk_organization_courseorg_id` (`org_id`),
  CONSTRAINT `organization_teache_org_id_cd000a1a_fk_organization_courseorg_id` FOREIGN KEY (`org_id`) REFERENCES `organization_courseorg` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of organization_teacher
-- ----------------------------
INSERT INTO `organization_teacher` VALUES ('1', '新垣结衣', '5', '日本演艺', '高级讲师', '幽默风趣', '0', '0', '2018-02-02 16:03:00.000000', '1', 'teacher/2018/02/mmexport1517233417475.jpg');
INSERT INTO `organization_teacher` VALUES ('2', '吴语茂', '3', 'xx', 'xx', 'xx', '0', '0', '2018-02-02 16:06:00.000000', '3', 'teacher/2018/02/-61d9a6865aaffeb8.jpg');
INSERT INTO `organization_teacher` VALUES ('3', '辛时', '0', '小米', '普通僵尸', '沉稳 死气', '0', '0', '2018-02-02 17:54:00.000000', '2', 'teacher/2018/02/7c5f51dbe5c576bb_hmIz1kN.jpg');

-- ----------------------------
-- Table structure for `users_banner`
-- ----------------------------
DROP TABLE IF EXISTS `users_banner`;
CREATE TABLE `users_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `index` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_banner
-- ----------------------------

-- ----------------------------
-- Table structure for `users_emailverifyrecord`
-- ----------------------------
DROP TABLE IF EXISTS `users_emailverifyrecord`;
CREATE TABLE `users_emailverifyrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `send_type` varchar(10) NOT NULL,
  `send_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_emailverifyrecord
-- ----------------------------
INSERT INTO `users_emailverifyrecord` VALUES ('2', 'admin', 'xxx@qq.com', 'register', '2018-01-12 14:30:00.000000');
INSERT INTO `users_emailverifyrecord` VALUES ('3', 'hehe', '134admin@163.com', 'forget', '2018-01-12 14:46:00.000000');
INSERT INTO `users_emailverifyrecord` VALUES ('4', 'ZDJ1T3LuSzYPeFB2', '15116490846@sina.com', 'register', '2018-01-17 11:15:39.388000');
INSERT INTO `users_emailverifyrecord` VALUES ('5', 'c6sa0KoDTFCKoBxl', '15116490846@sina.cn', 'register', '2018-01-17 11:19:59.749000');
INSERT INTO `users_emailverifyrecord` VALUES ('6', '3Ji8EO4JnXHqsBcD', '15116490846@sina.com', 'register', '2018-01-17 11:35:09.740000');
INSERT INTO `users_emailverifyrecord` VALUES ('7', 'cSq0I2qdfLEALNmy', '15116490846@163.com', 'register', '2018-01-17 11:39:29.470000');
INSERT INTO `users_emailverifyrecord` VALUES ('8', '9sQfRSMD2mbn6g0c', '15116490846@sina.cn', 'register', '2018-01-17 11:52:21.896000');
INSERT INTO `users_emailverifyrecord` VALUES ('9', 'rYoDzq7ghNSCD8Mn', '1345917401@qq.com', 'register', '2018-01-17 13:49:51.488000');
INSERT INTO `users_emailverifyrecord` VALUES ('10', '82vbBGi4ZUWNkeYc', '15116490846@163.com', 'register', '2018-01-17 13:53:37.857000');
INSERT INTO `users_emailverifyrecord` VALUES ('11', 'KKQHbaKEsBI45v0i', '15116490846@163.com', 'forget', '2018-01-21 12:51:46.366000');
INSERT INTO `users_emailverifyrecord` VALUES ('12', '32yVw8FK2ho5BgmW', '15116490846@163.com', 'forget', '2018-01-21 12:56:28.916000');
INSERT INTO `users_emailverifyrecord` VALUES ('13', '1uGVVnWUy8So97Ph', '15116490846@163.com', 'forget', '2018-01-21 13:01:58.704000');
INSERT INTO `users_emailverifyrecord` VALUES ('14', 'zSbUVFoxWWP0xCfr', '15116490846@163.com', 'forget', '2018-01-21 13:06:20.359000');
INSERT INTO `users_emailverifyrecord` VALUES ('15', '2C4ZXhjZGWqy15TT', '15116490846@163.com', 'forget', '2018-01-21 14:18:37.823000');

-- ----------------------------
-- Table structure for `users_userprofile`
-- ----------------------------
DROP TABLE IF EXISTS `users_userprofile`;
CREATE TABLE `users_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `nick_name` varchar(50) NOT NULL,
  `birday` date DEFAULT NULL,
  `gender` varchar(7) NOT NULL,
  `address` varchar(100) NOT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_userprofile
-- ----------------------------
INSERT INTO `users_userprofile` VALUES ('1', 'pbkdf2_sha256$24000$dJsLRHvjFnSg$H0DEiVerx+8nwz7P5CrNKCZC3NSjHrZnPYSZj4qAXBA=', '2018-04-02 11:19:00.000000', '1', 'admin', '', '', '1345917401@qq.com', '1', '1', '2018-01-12 05:07:00.000000', '贺贺', '2018-04-04', 'female', '长沙市', '', 'image/18/04/or.jpg');
INSERT INTO `users_userprofile` VALUES ('10', 'pbkdf2_sha256$24000$5siqhJnrAanM$QU1GXEZnM8J9xjguPrgmBCXFcpBDFz3GdJvnNnAOTW0=', '2018-01-21 14:34:00.000000', '0', '15116490846@163.com', '', '', '15116490846@163.com', '0', '1', '2018-01-17 13:53:00.000000', 'hehe', null, 'female', 'wu', '', 'image/18/01/aobama.png');
INSERT INTO `users_userprofile` VALUES ('11', 'pbkdf2_sha256$24000$YwSfwos0UNJm$gesK0XVP/+xt2esDJC9U1iFwJw+1Tyqq4kEbDC+IC7g=', null, '0', 'maxine', '', '', '', '0', '1', '2018-04-02 11:04:18.921000', '', null, 'female', '', null, 'courses/2018/02/mysql.jpg');
INSERT INTO `users_userprofile` VALUES ('12', 'pbkdf2_sha256$24000$117nu2EpnhJL$Pt5ZnKsyGsaIhzslOGAR1hjm/z+mfQxf5niU0eSCYL0=', null, '0', 'jack', '', '', '', '0', '1', '2018-04-02 11:05:01.555000', '', null, 'female', '', null, 'courses/2018/03/1348626500448.jpg');

-- ----------------------------
-- Table structure for `users_userprofile_groups`
-- ----------------------------
DROP TABLE IF EXISTS `users_userprofile_groups`;
CREATE TABLE `users_userprofile_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_userprofile_groups_userprofile_id_823cf2fc_uniq` (`userprofile_id`,`group_id`),
  KEY `users_userprofile_groups_group_id_3de53dbf_fk_auth_group_id` (`group_id`),
  CONSTRAINT `users_userprofil_userprofile_id_a4496a80_fk_users_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `users_userprofile` (`id`),
  CONSTRAINT `users_userprofile_groups_group_id_3de53dbf_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_userprofile_groups
-- ----------------------------

-- ----------------------------
-- Table structure for `users_userprofile_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `users_userprofile_user_permissions`;
CREATE TABLE `users_userprofile_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_userprofile_user_permissions_userprofile_id_d0215190_uniq` (`userprofile_id`,`permission_id`),
  KEY `users_userprofile_u_permission_id_393136b6_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `users_userprofil_userprofile_id_34544737_fk_users_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `users_userprofile` (`id`),
  CONSTRAINT `users_userprofile_u_permission_id_393136b6_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_userprofile_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `xadmin_bookmark`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_bookmark`;
CREATE TABLE `xadmin_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `url_name` varchar(64) NOT NULL,
  `query` varchar(1000) NOT NULL,
  `is_share` tinyint(1) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_bookma_content_type_id_60941679_fk_django_content_type_id` (`content_type_id`),
  KEY `xadmin_bookmark_user_id_42d307fc_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_bookma_content_type_id_60941679_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `xadmin_bookmark_user_id_42d307fc_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xadmin_bookmark
-- ----------------------------

-- ----------------------------
-- Table structure for `xadmin_log`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_log`;
CREATE TABLE `xadmin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `ip_addr` char(39) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` varchar(32) NOT NULL,
  `message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_log_content_type_id_2a6cb852_fk_django_content_type_id` (`content_type_id`),
  KEY `xadmin_log_user_id_bb16a176_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_log_content_type_id_2a6cb852_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `xadmin_log_user_id_bb16a176_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xadmin_log
-- ----------------------------
INSERT INTO `xadmin_log` VALUES ('1', '2018-01-12 14:38:19.719000', '127.0.0.1', '2', 'EmailVerifyRecord object', 'create', '已添加。', '8', '1');
INSERT INTO `xadmin_log` VALUES ('2', '2018-01-12 14:46:23.484000', '127.0.0.1', '3', 'hehe(134admin@163.com)', 'create', '已添加。', '8', '1');
INSERT INTO `xadmin_log` VALUES ('3', '2018-01-12 15:18:41.918000', '127.0.0.1', '1', 'Course object', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('4', '2018-01-12 15:20:57.413000', '127.0.0.1', '1', '第一节', 'create', '已添加。', '11', '1');
INSERT INTO `xadmin_log` VALUES ('5', '2018-01-12 18:08:08.659000', '127.0.0.1', '3', 'hehehehe', 'delete', '', '7', '1');
INSERT INTO `xadmin_log` VALUES ('6', '2018-01-12 18:19:37.628000', '127.0.0.1', '2', '三扥三扥所', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('7', '2018-01-12 18:26:46.875000', '127.0.0.1', '3', '阿东的', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('8', '2018-01-21 14:41:48.921000', '127.0.0.1', '10', '15116490846@163.com', 'change', '已修改 last_login，nick_name，address 和 image 。', '7', '1');
INSERT INTO `xadmin_log` VALUES ('9', '2018-01-23 14:34:31.581000', '127.0.0.1', '1', 'CityDict object', 'create', '已添加。', '14', '1');
INSERT INTO `xadmin_log` VALUES ('10', '2018-01-23 14:34:46.274000', '127.0.0.1', '2', 'CityDict object', 'create', '已添加。', '14', '1');
INSERT INTO `xadmin_log` VALUES ('11', '2018-01-23 14:35:00.148000', '127.0.0.1', '3', 'CityDict object', 'create', '已添加。', '14', '1');
INSERT INTO `xadmin_log` VALUES ('12', '2018-01-23 14:35:09.579000', '127.0.0.1', '4', 'CityDict object', 'create', '已添加。', '14', '1');
INSERT INTO `xadmin_log` VALUES ('13', '2018-01-23 14:35:24.041000', '127.0.0.1', '5', 'CityDict object', 'create', '已添加。', '14', '1');
INSERT INTO `xadmin_log` VALUES ('14', '2018-01-23 14:42:55.953000', '127.0.0.1', '1', 'CourseOrg object', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('15', '2018-01-23 14:56:29.185000', '127.0.0.1', '2', '清华大学', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('16', '2018-01-23 14:57:11.478000', '127.0.0.1', '3', '北京大学', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('17', '2018-01-23 14:57:59.021000', '127.0.0.1', '4', '慕课网2', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('18', '2018-01-23 14:58:23.822000', '127.0.0.1', '5', '慕课网3', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('19', '2018-01-23 14:58:51.625000', '127.0.0.1', '6', '慕课网4', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('20', '2018-01-23 14:59:18.956000', '127.0.0.1', '7', '慕课网5', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('21', '2018-01-23 14:59:43.168000', '127.0.0.1', '8', '慕课网666', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('22', '2018-01-23 15:00:59.086000', '127.0.0.1', '6', '南京市', 'create', '已添加。', '14', '1');
INSERT INTO `xadmin_log` VALUES ('23', '2018-01-23 15:01:37.464000', '127.0.0.1', '9', '南京市', 'create', '已添加。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('24', '2018-02-02 16:04:12.470000', '127.0.0.1', '1', 'Teacher object', 'create', '已添加。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('25', '2018-02-02 16:06:51.268000', '127.0.0.1', '2', '卫斯理', 'create', '已添加。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('26', '2018-02-02 16:08:30.260000', '127.0.0.1', null, '', 'delete', '批量删除 2 个 课程', null, '1');
INSERT INTO `xadmin_log` VALUES ('27', '2018-02-02 16:17:16.452000', '127.0.0.1', '1', 'Django入门', 'change', '已修改 course_org 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('28', '2018-02-02 16:57:17.227000', '127.0.0.1', '1', 'Django入门', 'change', '已修改 image 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('29', '2018-02-02 16:57:29.613000', '127.0.0.1', '1', 'Django入门', 'change', '没有字段被修改。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('30', '2018-02-02 17:24:29.238000', '127.0.0.1', '2', '卫斯理', 'change', '没有字段被修改。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('31', '2018-02-02 17:54:30.595000', '127.0.0.1', '3', '波比', 'create', '已添加。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('32', '2018-02-02 17:55:13.314000', '127.0.0.1', '4', 'java', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('33', '2018-02-02 18:12:16.647000', '127.0.0.1', '1', 'Django入门', 'change', '没有字段被修改。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('34', '2018-02-02 18:49:03.263000', '127.0.0.1', '1', '哈利波特', 'change', '已修改 image 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('35', '2018-02-02 18:49:26.662000', '127.0.0.1', '1', '哈利波特', 'change', '没有字段被修改。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('36', '2018-02-02 18:49:53.169000', '127.0.0.1', '1', '新垣结衣', 'change', '已修改 name 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('37', '2018-02-02 18:58:38.289000', '127.0.0.1', '2', '卫斯理', 'change', '已修改 image 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('38', '2018-02-02 18:59:05.886000', '127.0.0.1', '2', '李建丽', 'change', '已修改 name 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('39', '2018-02-02 19:00:00.175000', '127.0.0.1', '2', '李建丽', 'change', '已修改 image 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('40', '2018-02-02 19:00:04.277000', '127.0.0.1', '2', '李建丽', 'change', '没有字段被修改。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('41', '2018-02-02 19:00:52.144000', '127.0.0.1', '3', '傻子', 'change', '已修改 name 和 image 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('42', '2018-02-02 19:18:13.915000', '127.0.0.1', '9', '南京大学', 'change', '已修改 name 。', '15', '1');
INSERT INTO `xadmin_log` VALUES ('43', '2018-03-29 15:16:38.394000', '127.0.0.1', '5', 'go语言入门', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('44', '2018-03-29 15:21:11.205000', '127.0.0.1', '6', 'java中级', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('45', '2018-03-29 15:22:36.632000', '127.0.0.1', '7', 'python入门', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('46', '2018-03-29 16:24:50.228000', '127.0.0.1', '8', '大数据分析', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('47', '2018-03-29 16:25:44.804000', '127.0.0.1', '9', '机器学习', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('48', '2018-03-29 16:26:28.386000', '127.0.0.1', '10', '数据挖掘', 'create', '已添加。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('49', '2018-04-02 11:04:19.010000', '127.0.0.1', '11', 'maxine', 'create', '已添加。', '7', '1');
INSERT INTO `xadmin_log` VALUES ('50', '2018-04-02 11:05:01.664000', '127.0.0.1', '12', 'jack', 'create', '已添加。', '7', '1');
INSERT INTO `xadmin_log` VALUES ('51', '2018-04-02 12:03:22.482000', '127.0.0.1', '1', 'UserCourse object', 'create', '已添加。', '21', '1');
INSERT INTO `xadmin_log` VALUES ('52', '2018-04-04 02:53:23.230000', '127.0.0.1', '7', 'python入门', 'change', '已修改 tag 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('53', '2018-04-04 02:53:37.595000', '127.0.0.1', '1', 'Django入门', 'change', '已修改 tag 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('54', '2018-04-04 03:39:16.211000', '127.0.0.1', '2', '第一章 基础知识', 'create', '已添加。', '11', '1');
INSERT INTO `xadmin_log` VALUES ('55', '2018-04-04 03:39:31.761000', '127.0.0.1', null, '', 'delete', '批量删除 1 个 章节', null, '1');
INSERT INTO `xadmin_log` VALUES ('56', '2018-04-04 03:39:50.369000', '127.0.0.1', '3', '第二章 进阶开发', 'create', '已添加。', '11', '1');
INSERT INTO `xadmin_log` VALUES ('57', '2018-04-04 03:44:54.464000', '127.0.0.1', '1', '1.1 helloworld', 'create', '已添加。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('58', '2018-04-04 03:46:45.368000', '127.0.0.1', '2', '1.2 基本概念', 'create', '已添加。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('59', '2018-04-04 03:47:13.951000', '127.0.0.1', '3', '2.1 test', 'create', '已添加。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('60', '2018-04-04 03:47:27.814000', '127.0.0.1', '4', '2.2 test2', 'create', '已添加。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('61', '2018-04-04 07:44:27.973000', '127.0.0.1', '1', '前端页面', 'create', '已添加。', '13', '1');
INSERT INTO `xadmin_log` VALUES ('62', '2018-04-04 07:52:42.465000', '127.0.0.1', '5', 'Django高级应用之ORM', 'create', '已添加。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('63', '2018-04-04 07:52:55.025000', '127.0.0.1', '5', '2.3 Django高级应用之ORM', 'change', '已修改 name 。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('64', '2018-04-04 07:53:04.632000', '127.0.0.1', '5', '1.3 Django高级应用之ORM', 'change', '已修改 name 。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('65', '2018-04-04 07:56:29.439000', '127.0.0.1', '5', '1.3 Django高级应用之ORM', 'change', '已修改 url 。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('66', '2018-04-04 08:00:19.596000', '127.0.0.1', '5', '1.3 Django高级应用之ORM', 'change', '已修改 url 。', '12', '1');
INSERT INTO `xadmin_log` VALUES ('67', '2018-04-04 08:14:15.445000', '127.0.0.1', '1', 'Django入门', 'change', '已修改 teacher 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('68', '2018-04-04 08:18:59.744000', '127.0.0.1', '2', '吴语茂', 'change', '已修改 name 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('69', '2018-04-04 08:19:16.801000', '127.0.0.1', '3', '辛时', 'change', '已修改 name 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('70', '2018-04-04 08:20:01.076000', '127.0.0.1', '3', '辛时', 'change', '已修改 work_company，work_position 和 points 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('71', '2018-04-04 08:20:36.155000', '127.0.0.1', '1', '新垣结衣', 'change', '已修改 work_company，work_position 和 points 。', '16', '1');
INSERT INTO `xadmin_log` VALUES ('72', '2018-04-04 08:28:13.597000', '127.0.0.1', '1', 'Django入门', 'change', '已修改 youneed_know 和 teacher_tell 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('73', '2018-04-04 08:32:43.952000', '127.0.0.1', '1', 'Django入门', 'change', '已修改 youneed_know 和 teacher_tell 。', '10', '1');
INSERT INTO `xadmin_log` VALUES ('74', '2018-04-04 09:49:52.980000', '127.0.0.1', '1', 'admin', 'change', '已修改 last_login，nick_name，birday 和 address 。', '7', '1');
INSERT INTO `xadmin_log` VALUES ('75', '2018-04-04 09:50:23.672000', '127.0.0.1', '1', 'admin', 'change', '已修改 image 。', '7', '1');

-- ----------------------------
-- Table structure for `xadmin_usersettings`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_usersettings`;
CREATE TABLE `xadmin_usersettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(256) NOT NULL,
  `value` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_usersettings_user_id_edeabe4a_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_usersettings_user_id_edeabe4a_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xadmin_usersettings
-- ----------------------------
INSERT INTO `xadmin_usersettings` VALUES ('1', 'dashboard:home:pos', '', '1');
INSERT INTO `xadmin_usersettings` VALUES ('2', 'site-theme', '/static/xadmin/css/themes/bootstrap-xadmin.css', '1');

-- ----------------------------
-- Table structure for `xadmin_userwidget`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_userwidget`;
CREATE TABLE `xadmin_userwidget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` varchar(256) NOT NULL,
  `widget_type` varchar(50) NOT NULL,
  `value` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_userwidget_user_id_c159233a_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_userwidget_user_id_c159233a_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xadmin_userwidget
-- ----------------------------
